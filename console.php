#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
$app = new Application();
// Register commands in the application.
$app->add(new \AmericanArt\Scrap\Command\CsvToGeoJsonCommand());
$app->add(new \AmericanArt\Scrap\Command\ImportCsvEtixOptInsToMailChimp());
$app->run();
