<div align="center">
  <img src='/assets/images/icon.png' width="200" alt="Scrap Project Icon" style="max-width: 100%;" />
</div>

---

# Scrap
A development toolkit and CLI for parsing data and other utilities.

## Install
Clone the repository and install dependencies. Requires >= PHP 7.2

```shell script
composer install
```

## Commands
- Convert CSV to GeoJSON. A one-off script to convert a comma-delimited list of addresses to GeoJson
```shell script
php console.php convert:geo
```
- 