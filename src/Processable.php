<?php namespace AmericanArt\Scrap;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

trait Processable {

  /**
   * Run a Shell Command/Process.
   *
   * @param OutputInterface $output
   * @param string $command
   *   The shell command to run.
   * @param string $cwd
   *   The current working directory to run command.
   * @return mixed
   * @throws \Exception
   */
  protected function runProcess(OutputInterface $output, $command = '', $cwd = null) {
    $process = new Process([$command]);
    $wd = $cwd ? $cwd : '/';
    $process->setWorkingDirectory($wd);
    $process->setTimeout(900);
    $process->setIdleTimeout(360);
    try{
      $output->writeln(sprintf('<fg=white;bg=magenta;>[INFO] Executing: %s ...</>', $command));
      $process->run(function ($type, $data) use ($output, $command) {
          $output->writeln(sprintf('<fg=white;bg=magenta;>%s</>', $data));
      });
    }
    catch(\Exception $exception) {
      throw $exception;
    }
  }

}
