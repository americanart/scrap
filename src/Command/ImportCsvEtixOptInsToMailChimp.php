<?php

namespace AmericanArt\Scrap\Command;


use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use League\Csv\Reader;
use MailchimpMarketing\ApiClient;

class ImportCsvEtixOptInsToMailChimp extends Command {

    protected static $defaultName = 'import:etix';

    protected function configure() {
        $this->setDescription('Take a Customer Report from eTix and update an MailChimp List')
        ->addOption(
            'max',
            'm',
            InputOption::VALUE_OPTIONAL,
            'Limit number of items to process.',
            -1
        );
    }

    /**
     * Process a eTix csv file and import the opt-in.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->writeln(sprintf('<info>Running %s...</info>', $this->getName()));
        $max = $input->getOption('max');
        $client = new \GuzzleHttp\Client();

        // Get the CSV data.
        $csv = Reader::createFromPath($_ENV['ETIX_FILE'], 'r');
        $csv->setHeaderOffset(0);
        $records = $csv->getRecords();

        // Check the records to insure they are opted in and store to the email array.
        $emails = array();
        foreach ($records as $record) {
            if ((int) $max > 0 && $max <= sizeof($emails)) {
                $output->writeln(sprintf('Processed the maximum of %d lines of the import file', $max));
                break;
            }

//            var_dump($record);

            if($record['Opt In'] && $record['Opt In'] === 'Yes') {
                $firstName = "";
                $lastName = "";
                if($record['Customer User Name']) {
                    $email = $record['Customer User Name'];
                    if(!isset($emails[$email])) {
                        if($record['Order Shipping First Name']) {
                            $firstName = $record['Order Shipping First Name'];
                        }
                        if($record['Order Shipping Last Name']) {
                            $lastName = $record['Order Shipping Last Name'];
                        }
                        $emails[$email]  = array(
                            "email" => $email,
                            "firstName" => $firstName,
                            "lastName" => $lastName
                        );
                    }
                }
            }

//            var_dump($emails);
        }

        // Push the new email to the mailchimp list
        // todo: Add first and last names
        // todo: See what other info we can pull out of the eTix record
            $client = new ApiClient();
            $client->setConfig([
                'apiKey' => $_ENV['MAILCHIMP_API_KEY'],
                'server' => $_ENV['MAILCHIMP_SERVER']
            ]);


            // for each check to see if it exists
            // if it does and not set to subscribe then subscribe
            // if it does not, then add to the list.

            foreach ($emails as $email) {

//                print_r('Processing '.$email['email'].PHP_EOL);
                $subscriber_hash = md5(strtolower($email['email']));
//                print_r('    The Subscriber Hash is '.$subscriber_hash.' '.PHP_EOL);

                $inList = true;
                try {
                    $response = $client->lists->getListMember($_ENV['MAILCHIMP_LIST_ID'], $subscriber_hash);

//                    print_r('****** Response for List Check '.$email['email'].PHP_EOL);
//                    print_r($response);
//                    print_r('****** End of Response for List Check'.$email['email'].PHP_EOL);
                } catch (RequestException $e) {
//                    print_r($e->getMessage());
                    $errorMessage = $e->getMessage();
                    if(strpos($errorMessage, "404") !== false) {
                        $inList = false;
                        print_r($email['email'].' not in list'.PHP_EOL);
                    }
                } catch (Error $error) {
                    print_r($error->getMessage());
                }

                if($inList) {
                    try {
                        $response = $client->lists->updateListMember($_ENV['MAILCHIMP_LIST_ID'], $subscriber_hash, [
                            "status" => "subscribed",
                            "merge_fields" => [
                                "FNAME" => $email['firstName'],
                                "LNAME" => $email['lastName']
                            ]
                        ]);
//                        print_r($response);
                    } catch (Error $e) {
                        print_r($e->getMessage());
                    }
                } else {
                    try {
                        $response = $client->lists->addListMember($_ENV['MAILCHIMP_LIST_ID'], [
                            "email_address" => $email['email'],
                            "status" => "subscribed",
                            "merge_fields" => [
                                "FNAME" => $email['firstName'],
                                "LNAME" => $email['lastName']
                            ]
                        ]);
//                        print_r($response);
                    } catch (Error $e) {
                        print_r($e->getMessage());
                    }
                }
//                print_r($response); //debug fun
            }

        return Command::SUCCESS;
    }
}
