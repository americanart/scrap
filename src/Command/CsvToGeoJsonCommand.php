<?php

namespace AmericanArt\Scrap\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use League\Csv\Reader;

class CsvToGeoJsonCommand extends Command {

    protected static $defaultName = 'convert:geo';

    protected function configure() {
        $this->setDescription('Convert a CSV of Addresses into a valid GeoJSON')
        ->addOption(
            'max',
            'm',
            InputOption::VALUE_OPTIONAL,
            'Limit number of items to process.',
            -1
        );
    }

    /**
     * Convert a CSV File to GeoJSON and write to file.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->writeln(sprintf('<info>Running %s...</info>', $this->getName()));
        $max = $input->getOption('max');
        $client = new \GuzzleHttp\Client();

        // Get the CSV data.
        $csv = Reader::createFromPath(dirname(__DIR__, 2) . '/input/maps/fellows.csv', 'r');
        $csv->setHeaderOffset(0);
        $records = $csv->getRecords();

        // Set up the GeoJSON collection.
        $collection = [
            'type' => 'FeatureCollection',
            'features' => [],
        ];
        // Add each record to the GeoJSON collection as a "feature".
        $count = 0;
        foreach ($records as $record) {
            if ((int) $max > 0 && $max <= $count) {
                $output->writeln(sprintf('Processed the maximum of %d items', $max));
                break;
            }
            $feature = [
                'type' => 'Feature',
                'id' => $count + 1,
                'properties' => [],
                'geometry' => [],
            ];
            // Use the fellow's full name as title, which is either set in the "Salutation" field or combination
            // of FirstName, Middle, and LastName fields.
            if ($record['Salutation']) {
                $feature['properties']['name'] = $record['Salutation'];
            }
            else {
                $name = trim(implode(' ', [$record['FirstName'], $record['Middle'], $record['LastName']]));
                $feature['properties']['name'] = preg_replace('!\s+!', ' ', $name);
            }
            if ($record['Organization Name']) {
                $feature['properties']['organization'] = trim($record['Organization Name']);
            }
            // Get the lat/lng coordinates from Google's Geocoding API
            $address = urlencode(implode(' ', [$record['City'], $record['State'], $record['Country'], $record['ZIP Code']]));
            $url = sprintf('https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s', $address, $_ENV['GOOGLE_CLOUD_API_KEY']);
            $response = $client->request('GET', $url);
            if ($response->getStatusCode() === 200) {
                $payload = json_decode($response->getBody());
                if (property_exists($payload, 'results')) {
                    $location = $payload->results[0]->geometry->location;
                    $geometry = [
                        'type' => 'Point',
                        'coordinates' => [$location->lng, $location->lat],
                    ];
                    $feature['geometry'] = $geometry;
                    $collection['features'][] = $feature;
                }
            }
            $count++;
        }

        // Write the collection to a file.
        $json = json_encode($collection, JSON_PRETTY_PRINT);
        if (file_put_contents(dirname(__DIR__, 2) . '/output/fellows.json', $json)) {
            $output->writeln(sprintf('<fg=black;bg=white;>%s</>', 'Done'));
        }
        return Command::SUCCESS;
    }

}
